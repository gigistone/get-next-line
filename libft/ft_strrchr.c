/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 16:46:17 by bboutoil          #+#    #+#             */
/*   Updated: 2018/11/17 23:47:07 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char	*ft_strrchr(const char *str, int c)
{
	size_t	n;
	char	*res;

	res = NULL;
	n = ft_strlen(str) + 1;
	while (n > 0)
	{
		if ((unsigned char)*str == (unsigned char)c)
			res = (char *)str;
		str++;
		n--;
	}
	return (res);
}
