/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 11:06:18 by bboutoil          #+#    #+#             */
/*   Updated: 2018/11/25 15:27:13 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "libft.h"
#include "get_next_line.h"

static int	compare_fd_data(t_fd_data *fd_data, int *fd)
{
	return (fd_data->id == *fd ? 1 : 0);
}

/*
** APPEND_TO_LBUFFER
** desc: append data to buffer
** ret:  0 on success, GNL_ALLOC_ERROR on failure.
*/

static int	append_to_lbuffer(char **buffer, const char *str)
{
	char	*tmp;

	if (*buffer == NULL)
		*buffer = ft_strdup(str);
	else
	{
		tmp = *buffer;
		*buffer = ft_strjoin(*buffer, str);
		free(tmp);
	}
	if (*buffer == NULL)
		return (GNL_ALLOC_ERROR);
	return (0);
}

/*
** POP_FROM_BUFFER
** desc: pop from line buffer
** ret:  1 on success, 0 if buffer is empty, GNL_ALLOC_ERROR
** on mmemory alloc failure
*/

static int	pop_from_lbuffer(char **lbuff, char **output, size_t n)
{
	char	*tmp;

	if (n == 0)
	{
		if (*lbuff == NULL || **lbuff == '\0')
		{
			if (*lbuff != NULL)
				free(*lbuff);
			*output = NULL;
		}
		else
			*output = *lbuff;
		*lbuff = NULL;
		return (*output != NULL);
	}
	(*lbuff)[n - 1] = '\0';
	if ((*output = ft_strdup(*lbuff)) == NULL)
		return (GNL_ALLOC_ERROR);
	if ((tmp = ft_strdup(*lbuff + n)) == NULL)
		return (GNL_ALLOC_ERROR);
	ft_strdel(lbuff);
	*lbuff = tmp;
	return (1);
}

/*
** READ_LINE
** desc: internal function, read a line from a custom
** file descriptor buffer.
** ret:  1 on success, 0 if no line found, -1 on failure.
*/

static int	read_line(t_fd_data *fdd, char **line)
{
	char	buff[BUFF_SIZE + 1];
	int		bytes_read;
	char	*found;

	if (fdd->buffer != NULL && (found = ft_strchr(fdd->buffer, '\n')) != NULL)
	{
		if (pop_from_lbuffer(&fdd->buffer, line,
			(size_t)(found + 1 - fdd->buffer)) == -1)
			return (GNL_ERROR);
		return (GNL_SUCCESS);
	}
	if ((bytes_read = read(fdd->id, buff, BUFF_SIZE)) == -1)
		return (GNL_ERROR);
	if (bytes_read == 0)
	{
		if (pop_from_lbuffer(&(fdd->buffer), line, 0) == LINE_BUFFER_EMPTY)
			return (GNL_EOF);
		return (GNL_SUCCESS);
	}
	buff[bytes_read] = '\0';
	if (append_to_lbuffer(&(fdd->buffer), buff) == GNL_ALLOC_ERROR)
		return (GNL_ERROR);
	return (read_line(fdd, line));
}

/*
** GET_NEXT_LINE
** desc: read a line from a filedescriptor. this public function
** handles multi-fd.
** ret:  1 on success, 0 if no line found, -1 on failure.
*/

int			get_next_line(const int fd, char **line)
{
	static t_sglist	*fdd_lst = NULL;
	t_fd_data		*fdd;
	int				res;

	if (fdd_lst == NULL && (fdd_lst = ft_sglist_new_empty()) == NULL)
		return (GNL_ERROR);
	if ((fdd = ft_sglist_find(fdd_lst, &compare_fd_data, (int *)&fd)) == NULL)
	{
		if ((fdd = (t_fd_data *)malloc(sizeof(t_fd_data))) == NULL)
			return (GNL_ERROR);
		fdd->buffer = NULL;
		fdd->id = fd;
		ft_sglist_append(fdd_lst, fdd);
	}
	if ((res = read_line(fdd, line)) == GNL_EOF)
	{
		ft_sglist_remove(fdd_lst, fdd);
		free(fdd);
	}
	return (res);
}
