/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 10:20:48 by bboutoil          #+#    #+#             */
/*   Updated: 2018/11/24 10:58:39 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# define BUFF_SIZE			(3)
# define GNL_ALLOC_ERROR 	(-1)
# define LINE_BUFFER_EMPTY	(0)

enum			e_gnl_state
{
	GNL_ERROR = -1,
	GNL_EOF,
	GNL_SUCCESS
};

typedef struct	s_fd_data
{
	int			id;
	char		*buffer;
}				t_fd_data;

int				get_next_line(const int fd, char **line);

#endif
